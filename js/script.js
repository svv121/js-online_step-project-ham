"use strict";
/*
*/
/*-------------------------------------Section work - images--------------------------------------*/
const workImgSources = {
    'all': [
        'img/work/all/all1.jpg',
        'img/work/all/all2.jpg',
        'img/work/all/all3.jpg',
        'img/work/all/all4.jpg',
        'img/work/all/all5.jpg',
        'img/work/all/all6.jpg',
        'img/work/all/all7.jpg',
        'img/work/all/all8.jpg',
        'img/work/all/all9.jpg',
        'img/work/all/all10.jpg',
        'img/work/all/all11.jpg',
        'img/work/all/all12.jpg',
    ],
    'Graphic Design': [
        'img/work/graphic-design/graphic-design1.jpg',
        'img/work/graphic-design/graphic-design2.jpg',
        'img/work/graphic-design/graphic-design3.jpg',
        'img/work/graphic-design/graphic-design4.jpg',
        'img/work/graphic-design/graphic-design5.jpg',
        'img/work/graphic-design/graphic-design6.jpg',
        'img/work/graphic-design/graphic-design7.jpg',
        'img/work/graphic-design/graphic-design8.jpg',
        'img/work/graphic-design/graphic-design9.jpg',
    ],
    'Web Design': [
        'img/work/web-design/web-design1.jpg',
        'img/work/web-design/web-design2.jpg',
        'img/work/web-design/web-design3.jpg',
        'img/work/web-design/web-design4.jpg',
        'img/work/web-design/web-design5.jpg',
        'img/work/web-design/web-design6.jpg',
        'img/work/web-design/web-design7.jpg',
        'img/work/web-design/web-design8.jpg',
        'img/work/web-design/web-design9.jpg',
        'img/work/web-design/web-design10.jpg',
        'img/work/web-design/web-design11.jpg',
    ],
    'Landing Pages':
        [
            'img/work/landing-page/landing-page1.jpg',
            'img/work/landing-page/landing-page2.jpg',
            'img/work/landing-page/landing-page3.jpg',
            'img/work/landing-page/landing-page4.jpg',
            'img/work/landing-page/landing-page5.jpg',
            'img/work/landing-page/landing-page6.jpg',
            'img/work/landing-page/landing-page7.jpg',
            'img/work/landing-page/landing-page8.jpg',
            'img/work/landing-page/landing-page9.jpg',
            'img/work/landing-page/landing-page10.jpg',
            'img/work/landing-page/landing-page11.jpg',
            'img/work/landing-page/landing-page12.jpg',
            'img/work/landing-page/landing-page13.jpg',
            'img/work/landing-page/landing-page14.jpg',
        ],
    'Wordpress':
        [
            'img/work/wordpress/wordpress1.jpg',
            'img/work/wordpress/wordpress2.jpg',
            'img/work/wordpress/wordpress3.jpg',
            'img/work/wordpress/wordpress4.jpg',
            'img/work/wordpress/wordpress5.jpg',
            'img/work/wordpress/wordpress6.jpg',
            'img/work/wordpress/wordpress7.jpg',
            'img/work/wordpress/wordpress8.jpg',
            'img/work/wordpress/wordpress9.jpg',
            'img/work/wordpress/wordpress10.jpg',
        ],
};
const galleryImgSources = [
        'img/gallery/add-gallery19.jpg',
        'img/gallery/add-gallery20.jpg',
        'img/gallery/add-gallery21.jpg',
        'img/gallery/add-gallery22.jpg',
        'img/gallery/add-gallery23.jpg',
        'img/gallery/add-gallery24.jpg',
        'img/gallery/add-gallery25.jpg',
        'img/gallery/add-gallery26.jpg',
        'img/gallery/add-gallery27.jpg',
        'img/gallery/add-gallery28.jpg',
        'img/gallery/add-gallery29.jpg',
        'img/gallery/add-gallery30.jpg',
    ];

/*------------------------Tabs Services Changer------------------------*/
let tabTitleActive = document.querySelector('.services-tabs-title.active');
const triangle = document.createElement('div');
triangle.classList.add('services-triangle');
tabTitleActive.append(triangle);
const tabsWithTexts = document.querySelector(".centered-content");
const tabTitles = document.querySelectorAll(".services-tabs-title");
const tabTexts = document.querySelectorAll(".services-item");
tabsWithTexts.addEventListener("click", (event) => {
    const id = event.target.dataset.id;
    if (id) {
        tabTitles.forEach(title => {
            title.classList.remove("active");
        });
        event.target.classList.add("active");
        tabTitleActive = event.target;
        tabTitleActive.classList.add('active');
        tabTitleActive.append(triangle);
        tabTexts.forEach(text => {
            text.classList.remove("active");
        });
        const element = document.getElementById(id);
        element.classList.add("active");
    }
})

/*---------------------filter--------------------*/
let counter = 0;
const sectionWork = document.querySelector('.work-images');
const btnWork = document.querySelector('.btn-work');

const filterArray = (imgSources, tab) => {
    const categoryImg = imgSources[tab];
    if (categoryImg) {
        return categoryImg;
    } else {
        let allImg=[];
        for (let key in imgSources) {
            let categoryKey=imgSources[key];
            allImg=allImg.concat(categoryKey);
        }
        return allImg;
    }
}
const createElements = (allImgList, number, tab) => {
    if (allImgList.length<number) {
        $(btnWork).hide();
    } else {
        $(btnWork).show();
    }
    allImgList.forEach((imgSrc, index)=> {
        if (index <= (number - 1)) {
            if (tab === undefined) {
                sectionWork.innerHTML += `<div class="work-img-card" data-category = "${tab}">
                 <div class="work-img-card-inner">
                    <div class="work-img-card-front">
                    <img class = "work-img" src = ${imgSrc} alt="">
                    </div>
                        <div class = "work-img-card-back">
                            <button class = "btn-link icon"></button>
                            <button class = "btn-search icon"></button>
                            <h3 class = "img-title">Creative Design</h3>
                            <span class = "img-category">Our works</span>
                        </div>
                    </div>
                </div>`
            }
            else {
                sectionWork.innerHTML += `<div class="work-img-card" data-category = "${tab}">
                   <div class="work-img-card-inner">
                        <div class="work-img-card-front">
                            <img class = "work-img" src = ${imgSrc} alt = "">
                        </div> 
                        <div class = "work-img-card-back">
                            <button class="btn-link icon"></button>
                            <button class="btn-search icon"></button>
                            <h3 class="img-title">Creative Design</h3>
                            <span class="img-category">${tab}</span>
                        </div>
                   </div>
                </div>`
            }
        }
    })
}
function cleanArray (allImgList, number) {
    for(let i=0; i <= (number-1); i++){
        allImgList.shift();
    }
    if(allImgList.length <= number) {
        $(btnWork).hide();
    } else {
        $(btnWork).show();
    }
    return allImgList;
}
let allImages=filterArray(workImgSources,undefined);
createElements(allImages,12,undefined);

/*-----------------Tabs Work Changer-----------------*/
let tabName = undefined;
$('.work-category-item').on('click', function () {
    const activeItem = $('.work-category-item.active');
    activeItem.removeClass('active');
    $(this).addClass('active');
    tabName = $(this).attr("data-category");
    sectionWork.innerHTML = '';
    allImages = filterArray(workImgSources,tabName);
    createElements(allImages,12, tabName);
    counter = 0;
    return tabName;
});

/*-----------------Work load more btn-----------------*/
btnWork.addEventListener('click', ()=>{
    counter += 1;
    $(btnWork).hide();
    $('.loading').show();
    setTimeout(() => {
        allImages = cleanArray(allImages,12);
        createElements(allImages,12,tabName);
        if (counter === 2 || allImages.length <= 12) {
            $(btnWork).hide();
        } else {
            $(btnWork).fadeIn();
        }
        $('.loading').hide();
    },2000);
});

/*---------------------Slider Slick------------------------*/
$('.testimonials-carousel').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.respondents-nav'
});
$('.respondents-nav').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.testimonials-carousel',
    dots: false,
    centerMode: true,
    focusOnSelect: true,
    autoplay:true,
    autoplaySpeed: 5000
});

/*---------------------Masonry------------------------*/
const newImages = galleryImgSources.map(item => `<div class="gallery-item grid-item">
    <img class="gallery-img" src="${item}" alt="">
        <div class="gallery-item-overlay">
            <button class="btn-img-search fas fa-search gallery-hover-item hover-item"></button>
            <button class="btn-img-zoom fa fa-arrows-alt gallery-hover-item hover-item"></button>
        </div>
     </div>`
);
const $grid = $('.grid');
$(function() {
    $grid.masonry({
        isFitWidth: true,
        itemSelector: '.grid-item',
        columnWidth: 370,
        gutter: 16,
        stagger: 200
    });
});

/*-----------------Gallery load more btn-----------------*/
$('.btn-gallery').on('click', function() {
    const el = $(newImages.join(''));
    $('.btn-gallery').hide();
    $('.loading2').show();
    setTimeout(() => {
            $grid.masonry().append(el).masonry('appended', el);
        $('.loading2').hide();
    },2000);
})

/*-------------------------Arrow-up--------------------------*/
const upButton = document.querySelector("#arrow-up")
const handleScroll = () => {
    if(window.scrollY >= document.documentElement.clientHeight){
        upButton.style.display = "flex"
    } else {
        upButton.style.display = "none"
    }
};
handleScroll();
window.addEventListener('scroll',  handleScroll);
upButton.addEventListener("click", () => {
    window.scrollTo({
        top: 0,
        behavior: "smooth",
    })
})

/*--------------------Set_year_on_footer---------------------*/
window.addEventListener('load', () => {
        document.getElementById('copyright-year').append(
            document.createTextNode(new Date().getFullYear().toString()
            )
        );
    }
);